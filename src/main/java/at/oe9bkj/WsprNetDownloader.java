package at.oe9bkj;

import org.ccil.cowan.tagsoup.jaxp.SAXParserImpl;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;

/**
 * Java command line tool for downloading WSPRNet results (http://wsprnet.org) to a CSV result for analyzing purpose
 * This class uses the "old" database interface http://wsprnet.org/olddb
 * The result from the web-page is a HTML table, which is parsed with org.ccil.cowan.tagsoup.jaxp.SAXParserImpl
 * To be started on a command line, see usage().
 * You can redirect the output to a (CSV)-file by redirecting the output into a file (e.g. .... > result.csv)
 *
 * @author Copyright 2019, OE9BKJ, K. Battlogg
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class WsprNetDownloader {
    private static final boolean debug = false; // turn to true, if you want raw results for debugging purpose
    protected static StringBuffer sb = new StringBuffer(); // SB for result data
    public final static int TABLE_CNT = 3; // this table in the HTML result contains the data
    public final static int TABLE_TR_CNT = 3; // skip some table rows for the data table
    public final static int DEFAULT_LIMIT = 50; // display how many rows?
    public final static String DEFAULT_DELIMITER = ";"; // CSV delimiter character; change if you want another delimiter

    // available bands on command line
    public final static String[] BANDS = {"all", "2190", "160", "80", "60", "40", "30", "20", "17", "15", "12", "10", "6", "4", "2", "220", "432"};
    // available sort fields on command line
    public final static String[] SORT = {"date", "mhz", "distance", "db", "callsign"};
    // Table header in CSV result
    public final static String HEADER = "\"Date\"" + DEFAULT_DELIMITER + "\"Call\"" + DEFAULT_DELIMITER + "\"Freq.\"" + DEFAULT_DELIMITER + "\"SNR\""
            + DEFAULT_DELIMITER + "\"Grid\"" + DEFAULT_DELIMITER + "\"dBm\"" + DEFAULT_DELIMITER + "\"W\"" + DEFAULT_DELIMITER + "\"by\""
            + DEFAULT_DELIMITER + "\"loc\"" + DEFAULT_DELIMITER + "\"km\"" + DEFAULT_DELIMITER + "";

    /* Result format - with DEFAULT_DELIMITER
    Date              Call    Freq. 	 SNR  Grid  dBm  W 	      by 	  loc     km
    2019-09-13 13:42; 0J1CHM; 14.097042; -17; NF42; +60; 1000.0; OE9BKJ; JN47ul; 12278;
     */
    public final static Integer[] COLUMNS_TO_DISPLAY = {1, 3, 5, 7, 11, 13, 15, 17, 19, 21}; // column numbers of result table from WSPRNet

    /**
     * Main method for command line usage. See usage();
     * If you start without or with to many params, you will get a usage-note output.
     *
     * @param args <Call> <Reporter> <band> [limit] [unique] [reverse] [sort]
     * @throws IOException  Problems with http-Connection
     * @throws SAXException Problems with parsing the result HTML
     */
    public static void main(String[] args) throws IOException, SAXException {
        String call, reporter, band;
        String sort = "date";
        int limit = DEFAULT_LIMIT;
        boolean unique = false;
        boolean reverse = false;

        // Check arguments and set default values
        if (args.length <= 2 || args.length > 7)
            System.out.println(usage()); // display usage
        else {
            // parse mandatory arguments
            call = args[0];
            if (call.length() < 3) call = "";

            reporter = args[1];
            if (reporter.length() < 3) reporter = ""; // set empty if less than 3 chars
            band = args[2];
            if (!Arrays.asList(BANDS).contains(band)) {
                System.out.println("Wrong band name.\n " + usage()); // display usage
                System.exit(1);
            }

            //parse optional arguments
            if (args.length >= 4) {
                try {
                    limit = Integer.parseInt(args[3]);
                } catch (Exception e) {
                    limit = DEFAULT_LIMIT;
                }
            }
            if (limit < 0) limit = DEFAULT_LIMIT;
            if (limit > 999) limit = 999;

            if (args.length >= 5) {
                if ("true".equalsIgnoreCase(args[4]))
                    unique = true;
            }
            if (args.length >= 6) {
                if ("true".equalsIgnoreCase(args[5]))
                    reverse = true;
            }
            if (args.length == 7) {
                sort = args[6];
                if (!Arrays.asList(SORT).contains(sort)) {
                    System.out.println("Wrong sort qualifier.\n " + usage()); // display usage
                    System.exit(1);
                }
            }
            try {
                System.out.println(HEADER + getWSPRSpot(call, reporter, band, limit, unique, reverse, sort));
            } catch (ConnectException e) {
                System.out.println("WSPRNet.org responding slow or bad internet connection. Please try later\n" + e.getLocalizedMessage());
            }

        }
    }

    /**
     * Return some usage-notes
     *
     * @return String with usage-notes
     */
    private static String usage() {
        String usage = "Command line tool for downloading WSPRNet results (http://wsprnet.org) by OE9BKJ\n\n" +
                "----------------------------------------------------------------------------------\n" +
                "usage on command line with arguments: <Call> <Reporter> <band> [limit] [unique] [reverse] [sort]\n" +
                " executable jar:\n" +
                "  java -jar wsprnet.jar . OE9BKJ all > d:\\temp\\results.csv\n" +
                " or by executing the Java class:\n" +
                "  java at.oe9bkj.WsprNetDownloader <Call> <Reporter> <band> [limit] [unique] [reverse] [sort]\n" +
                "e.g. java -jar wsprnet.jar . OE9BKJ all 50 true false\n" +
                "     java -jar wsprnet.jar 0J1CHM OE9BKJ all\n" +
                "     java -jar wsprnet.jar . OE9BKJ all 10 false true mhz > results.csv\n" +
                "\n" +
                "Mandatory arguments:\n" +
                " Call.........TX Callsign \n" +
                " Reporter.....RX Callsign \n" +
                " band.........all|2190|160|80|60|40|30|20|17|15|12|10|6|4|2|220|432 - frequency band from 2190m..2m to 432Mhz (70cm)\n\n" +
                "Optional arguments:\n" +
                " limit........Number of Results (default = 50, 1..999)\n" +
                " unique.......true|false, default = false; Show only unique calls; this might cause other functions not working as expected!\n" +
                " reverse......true|false, default = false; Show results in reverse order;\n" +
                " sort.........date|mhz|distance|db|callsign - sort order\n\n" +
                "Results will be displayed in comma separated form (CSV), delimiter is ';'\n" +
                "Use '>' to redirect output from console to a file...\n" +
                "Feedback to oe9bkj@oevsv.at <oe9bkj@oevsv.at> welcome!\n\n" +
                "Please note: if you get a 'java.net.ConnectException: Connection timed out: connect'\n" +
                "then WSPRNet.org is not responding - which happens quite often - or your internet connection is bad.";
        return usage;
    }

    /**
     * This method will do the http-connection to http://wsprnet.org/olddb
     *
     * @param call     - the TX call
     * @param reporter - the RX call
     * @param band     - the band, see usage()
     * @param limit    - limit the number of results, see usage()
     * @param unique   - return only unique results? default=false, see usage(); this might cause other functions not to work as expected, due to a bug in WSPRNet.org
     * @param reverse  - use reverse sort order (default=false), see usage()
     * @param sort     - use sort field, (default=date)
     * @return CSV result
     * @throws IOException  Problems with http-Connection
     * @throws SAXException Problems with parsing the result HTML
     */
    public static String getWSPRSpot(String call, String reporter, String band, int limit, boolean unique,
                                     boolean reverse, String sort) throws IOException, SAXException {
        URL httpurl = null;
        URLConnection conn = null;
        String url;
        String response = "reponse";

        url = "http://wsprnet.org/olddb?mode=html";
        url += "&band=" + band;
        url += "&limit=" + limit;
        url += "&findcall=" + call.toUpperCase();
        url += "&findreporter=" + reporter.toUpperCase();
        url += "&sort=" + sort;
        if (reverse)
            url += "&reverse=on";

        if (unique)
            url += "&unique=on";

        try {
            if (debug) { //show raw result in debug-mode
                httpurl = new URL(url);
                conn = httpurl.openConnection(); // open the http connection
                // open the stream and put it into BufferedReader
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(conn.getInputStream()));
                StringBuffer inputBuffer = new StringBuffer();
                String inputLine = null;

                while ((inputLine = br.readLine()) != null) {
                    inputBuffer.append(inputLine); //read the result into a StringBuffer
                }

                response = inputBuffer.toString();
                System.out.println("response=" + response);
            }
            //try to parse the http input stream
            SAXParserImpl.newInstance(null).parse(
                    new URL(url).openConnection().getInputStream(),
                    new DefaultHandler() {
                        String tag = "";
                        String value = "";
                        int table_cnt = 0;
                        int table_tr_cnt = 0;
                        int table_tr_inserted_cnt = 0;
                        int table_td_cnt = 0;

                        // Element handling for "table"
                        public void startElement(String uri, String localName,
                                                 String name, Attributes a) {
                            if (name.equalsIgnoreCase("table")) table_cnt++; //start parsing the HTML table
                            if (table_cnt == TABLE_CNT && table_tr_cnt < limit + TABLE_TR_CNT) {
                                if (name.equalsIgnoreCase("tr")) table_tr_cnt++; // another table row
                                if (table_tr_cnt >= TABLE_TR_CNT) {
                                    if (name.equalsIgnoreCase("tr")) {
                                        if (table_tr_inserted_cnt > 0) {
                                            sb.append("\n");
                                            table_td_cnt = 0; // reset column count for next row
                                        } else
                                            sb.append("\n");
                                        table_tr_inserted_cnt++;
                                    }
                                    if (debug) System.out.println("OPEN TAG: " + name + " | " + localName);
                                }
                                tag = name;
                                if (name.equalsIgnoreCase("div")) table_tr_cnt = 0; // End of Table
                            }
                        }

                        // Column handling
                        public void characters(char ch[], int start, int length)
                                throws SAXException {
                            if (table_cnt == TABLE_CNT) {
                                // WSPRnet ignores limit, when unique is set ...
                                if (table_tr_cnt >= TABLE_TR_CNT && table_tr_cnt < limit + TABLE_TR_CNT) {
                                    table_td_cnt++;
                                    if (Arrays.asList(COLUMNS_TO_DISPLAY).contains(table_td_cnt)) { //display only selected td columns with the WSPRnet results
                                        value = String.valueOf(Arrays.copyOfRange(ch, start + 1, start + length)).trim();
                                        if (table_td_cnt == 15) // Power, cut decimals to 9.9
                                        {
                                            try {
                                                value = value.substring(0, value.indexOf(".") + 2) + "";
                                            } catch (Exception e) {
                                            }
                                        }

                                        if (table_td_cnt == 21) // distance
                                            value += "";
                                        if ((table_td_cnt == 3 || table_td_cnt == 17) // call or reporter found
                                                && (call.toUpperCase().equalsIgnoreCase(value) || reporter.toUpperCase().equalsIgnoreCase(value))) {
                                            sb.append(" ").append(value).append(DEFAULT_DELIMITER);
                                        } else {
                                            sb.append(" ").append(value).append(DEFAULT_DELIMITER);
                                        }
                                    }
                                }
                            }
                        }
                    }
            );
        } catch (MalformedURLException e) {
            e.printStackTrace();
            response = "MalformedURLException";
        } catch (IOException e) {
            e.printStackTrace();
            response = "IOException";
        } catch (SAXException e) {
            e.printStackTrace();
            response = "SAXException/parsing";
        }

        response = sb.toString();
        if (debug) System.out.println("response=" + response);

        return response;
    }
}
